import { Link } from "react-router-dom";

function Home() {
  return (
    <div className="menu">
      <h1>Crushy Candy</h1>
      <nav>
        <Link className="play" to="/game">
          Jouer
        </Link>
        <Link className="rules" to="/rules">
          Règles
        </Link>
      </nav>
    </div>
  );
}

export default Home;
