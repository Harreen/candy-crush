import { Routes, Route } from "react-router-dom";

import GameScreen from "./pages/GameScreen";
import Home from "./pages/Home";
import Rules from "./components/Rules";

function App() {
  return (
    <Routes>
      <Route path="/" />
      <Route index element={<Home />} />
      <Route path="game" element={<GameScreen />} />
      <Route path="rules" element={<Rules />} />
    </Routes>
  );
}

export default App;
