function reload() {
  function refreshPage() {
    window.location.reload(false);
  }

  return (
    <div className="refresh">
      <button onClick={refreshPage}>Refresh</button>
    </div>
  );
}
export default reload;
