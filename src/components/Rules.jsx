import { Link } from "react-router-dom";

const Rules = () => {
  return (
    <div className="Rules">
      {/* <div className="popup">
        <h2>Règles</h2>
        <p>Le but de ce jeu est de faire un maximum de points en déplaçant les bonbons pour faire un combo de 3 ou 4.</p>
        <p>Les combos se font à la vertical ou l'horizontal. Un point est attribué pour chaque bonbon disparu.</p>
        <p>Pour déplacer un bonbon, cliquez dessus, et effectuez un drag & drop avec un bonbon adjaçant.</p>
        <p>La 2nde règle : amusez-vous !</p>
      </div> */}
      <h2>
        Règles
      </h2>
      <ul>
        <li>Le but de ce jeu est de faire un maximum de points en déplaçant les bonbons pour faire un combo de 3 ou 4.</li>
        <li>Les combos se font à la vertical ou l'horizontal. Un point est attribué pour chaque bonbon disparu.</li>
        <li>Pour déplacer un bonbon, cliquez dessus, et effectuez un drag & drop avec un bonbon adjaçant.</li>
        <li>La 2nde règle : amusez-vous !</li>
      </ul>
      <Link className="close-rules" to="/">
        X
      </Link>
    </div>
  );
};

export default Rules;
