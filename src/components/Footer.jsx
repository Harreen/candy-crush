import { Link } from "react-router-dom";

const Footer = () => {
  return (
    <footer className="footer">
      <nav>
        <Link to="/">Accueil</Link>
      </nav>
    </footer>
  );
};

export default Footer;
